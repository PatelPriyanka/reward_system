<h2 align="center">Secure Reward System</h2>

<h4 align="center">Introducation</h4>
<div>
<p  text-align="justify">
Nowadays, larger Organizations are using reward point concepts to attract customers. The giant grocery stores are using this trek to increase their profit and sales instead of marketing themselves. However, for the current scenario, the customer can earn and redeem the points within a specific organization. They cannot earn and redeem in any other organization. The concept of interoperability; means earn and redeem the point in cross-organization is not exists yet. The reward system is not global in such a way that it allows the large and small Organizations to register themselves in the system and provide a reward platform for the customer. To address this challenge, this project proposes a GY-Reward system. That means to get your Reward (GY-Reward). GY-Reward system will welcome different companies and encourage them to join the GY-Reward (get your Reward) network. Companies will get secured ready Platform, where there can give Reward to their loyal customer. This system is customer-oriented, where the loyal customer will get rewarded for their daily purchases from the store or specific company which are part of this system. The Reward is a point in this application. The customer can buy products by redeeming the collected points. The GY-Reward system is responsible for monitoring the activity of all the users and the grocery stores/ organization. This application is mainly benefitting those grocery stores who would like to attract the customer or give the Reward to the faithful customer. This application is secure because it uses the blockchain concept for point transaction and permission network concept where each entity has an identity.
</p>
</div>


<p align="center"> Few lines describing your project.
    <br> 
</p>

## 📝 Table of Contents

- [Flow of syatem](#flow)
- [Module of Project](#module)
- [How to run the code?](#run)
- [Built Using](#built_using)
- [Result](#result)

## 🧐 Flow of System <a name = "flow"></a>
<div>
![Flow Diagram](https://gitlab.com/PatelPriyanka/reward_system/-/raw/master/Image/flowChart.png)
<p  text-align="justify">
The diagram represents the flow of the GY-reward system. Here, the admin is responsible for monitoring the whole system. The Organization needs to register in the network to become a part of the network. Before registration, they need to verify themselves and give proper ID proof to get an access card. Once they get an access card, they need to register through the registration panel. Once they register, they will have a public key and a private key. Once they register, they will become a part of the network and have identification of the network. The Organization now log in and can see the transaction details. The Organization can now give the deal of points on product purchases. So, it is up to the Organization that how many points they want to give on specific product purchases. In the Diagram, we can see there is one more entity customer. The customer needs to have an access card Id from Organization or GY-Reward. Once they get the access card, they can register by using the customer registration page. After registration through the log-in panel, they can see a good deal given by the grocery store on purchase. They can purchase the products and earn points on purchases. The customer can redeem the points by redeeming the points. The customer can see their transaction history through the panel. 
</p>
</div>


## 🏁 Module of Project <a name = "module"></a>
### 1 System Admin: 
- When we would like to run the application for the 1st time, we need to register the admin by using the enrollAdmin.js file 
- The system admin can check How much point is borrowed and used by the Organization and give it to their customer as a Reward 
- Check how much customers redeem into a specific Organization. Keep a record of everything. The main task is to monitor organization and customer Transection and make sure no one temper with transaction records. 

### 2 Organization Module (Grocery store):
- The application allows the Organization to register itself on the network and become a part of the reward system. New organizations can join the reward system, but before joining, they need to verify themselves from the higher authority and get an access card to register on the network. 
- During the registration process, they get:
1. Card ID: Get Private and public key with card id. (log in)
2. PIN (Personal identification number - Secret):  Identification on the network 
- They will get Platform:
o	Offer Reward Points on the purchase of products 
o	Provide a good deal to redeem the customer points.
- Note (Terms and conditions): The Organization needs to borrow the points from the GY-Reward administrator to give a Reward. An organization can only see the transaction history of Earn and redeem points on their Organization. They can’t see the transaction history of other Organizations.

### 3 Customer Module:
- Customer Identity on the network: The customer needs to get an Access card from Organization or from the GY-Reward system to get register on the network. They will get a Card ID with the secret PIN (personal identification number). By using these details, customers become part of the network. 
- Once they registered, they can log in and start earning the point on their purchase. Customers can Redeem the points to buy any products from the available stores. 
- Transaction verification: Customer can see the transaction ID under the transaction history, which will be used to verify the transaction-block on the network at any time.
- Security Parameters: The customer can see their transaction in history; they do not have access to other customer’s transaction history. The user password is encrypted by SHA; it will enhance security.

## 🔧 How to run the code? <a name = "run"></a>

To run the code just follow the steps given in this file. Link: <a>https://gitlab.com/PatelPriyanka/reward_system/-/blob/master/StepsToRun%20Gitlab%20code.pdf</a>

## ⛏️ Built Using <a name = "built_using"></a>

- [Ubuntu](https://ubuntu.com/) - Operating System
- [Hyperledger Fabric](https://hyperledger-fabric.readthedocs.io/en/release-2.2/whatis.html) - Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment
- [VueJs](https://vuejs.org/) - Web Framework
- [Express](https://expressjs.com/)
- [HTML](https://www.w3schools.com/html/)
- [Bootstrap](https://getbootstrap.com/)
- [Docker](https://www.docker.com/)

## 🚀 Result <a name = "result"></a>

<h3>
<b>
- To see more Result related [Images](https://gitlab.com/PatelPriyanka/reward_system/-/tree/master/Image)
</b>
</h3>

<div>
- Home Page
![Web Application](https://gitlab.com/PatelPriyanka/reward_system/-/raw/master/Image/webPage.png)

<br>
- How to Earn Reward
![How to Earn Reward](https://gitlab.com/PatelPriyanka/reward_system/-/raw/master/Image/howToEarn.png)
</div>