'use strict';

//stackoverflow
function isInt(value) {
    return !isNaN(value) && (function(x) {
        return (x | 0) === x;
    })(parseFloat(value));
}

//stackoverflow
function validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//stackoverflow
function validatePhoneNumber(phoneNumber) {
    let re = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/;
    return re.test(String(phoneNumber));
}

module.exports = {

    validateMemberRegistration: async function(cardId, accountNumber, firstName, lastName, email, phoneNumber, pcode, password) {

        let response = {};

        //verify input otherwise return error with an informative message
        if (accountNumber.length < 6) {
            response.error = 'Account PIN number must be six digits long';
            console.log(response.error);
            return response;
        } else if (!isInt(accountNumber)) {
            response.error = 'Account PIN number must be all numbers';
            console.log(response.error);
            return response;
        } else if (accountNumber.length > 6) {
            response.error = 'Account PIN number must be 6 digits';
            console.log(response.error);
            return response;
        } else if (cardId.length > 6) {
            response.error = 'Access key must be 6 digits';
            console.log(response.error);
            return response;
        } else if (!/^[0-9]+$/.test(cardId)) {
            response.error = 'Card id can be numbers only';
            console.log(response.error);
            return response;
        } else if (firstName.length < 1) {
            response.error = 'Enter first name';
            console.log(response.error);
            return response;
        } else if (!/^[a-zA-Z]+$/.test(firstName)) {
            response.error = 'First name must be letters only';
            console.log(response.error);
            return response;
        } else if (lastName.length < 1) {
            response.error = 'Enter last name';
            console.log(response.error);
            return response;
        } else if (!/^[a-zA-Z]+$/.test(lastName)) {
            response.error = 'First name must be letters only';
            console.log(response.error);
            return response;
        } else if (email.length < 1) {
            response.error = 'Enter email';
            console.log(response.error);
            return response;
        } else if (!validateEmail(email)) {
            response.error = 'Enter valid email';
            console.log(response.error);
            return response;
        } else if (phoneNumber.length < 10) {
            response.error = 'Enter phone number (Must be 10 digits)';
            console.log(response.error);
            return response;
        } else if (!validatePhoneNumber(phoneNumber)) {
            response.error = 'Enter valid phone number';
            console.log(response.error);
            return response;
        } else {
            console.log('Valid Entries');
            return response;
        }

    },

    validatePartnerRegistration: async function(cardId, partnerId, name) {

        let response = {};

        //verify input otherwise return error with an informative message
        if (cardId.length < 8) {
            response.error = 'Access Card ID must be 8 digits';
            console.log(response.error);
            return response;
        } else if (cardId.length > 8) {
            response.error = 'Access Card ID must be 8 digits';
            console.log(response.error);
            return response;
        } else if (!/^[0-9]+$/.test(cardId)) {
            response.error = 'Access key ID must be numbers only';
            console.log(response.error);
            return response;
        } else if (partnerId.length < 8) {
            response.error = 'Organization ID must be Eight Digits';
            console.log(response.error);
            return response;
        } else if (partnerId.length > 8) {
            response.error = 'Organization ID must be Eight Digits';
            console.log(response.error);
            return response;
        } else if (!/^[0-9]+$/.test(partnerId)) {
            response.error = 'Partner id must be numbers only';
            console.log(response.error);
            return response;
        } else if (name.length < 1) {
            response.error = 'Enter company name';
            console.log(response.error);
            return response;
        } else if (!/^[a-zA-Z]+$/.test(name)) {
            response.error = 'Company name must be letters only';
            console.log(response.error);
            return response;
        } else {
            console.log('Valid Entries');
            return response;
        }

    },

    validatePoints: async function(points) {

        let response = {};
        if (isNaN(points)) {
            response.error = 'Points must be number';
            console.log(response.error);
            return response;
        } else {
            return Math.round(points);
        }

    }

};