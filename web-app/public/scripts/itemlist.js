
export default function findData(a){
    const idA= a;
    const res = itemDB.filter(item => item.id && item.id.toString() === a);
    return res;
}

// exports.findData = findData;
const itemDB = [{
    id: 1,
    itemName: 'Strawberry 2LB',
    itemEarnPoints: 200,
    itemRedeemPoints: 600,
    itemPrice: 6.00
},
{
    id: 2,
    itemName: 'Pineapple 1',
    itemEarnPoints: 200,
    itemRedeemPoints: 500,
    itemPrice: 5.00
},
{
    id: 3,
    itemName: 'Apple 3LB',
    itemEarnPoints: 300,
    itemRedeemPoints: 700,
    itemPrice: 7.00
},
{
    id: 4,
    itemName: 'Banana',
    itemEarnPoints: 150,
    itemRedeemPoints: 300,
    itemPrice: 3.00
},
{
    id: 5,
    itemName: 'Lettuce EA',
    itemEarnPoints: 150,
    itemRedeemPoints: 380,
    itemPrice: 3.80
},
{
    id: 6,
    itemName: 'Kale EA',
    itemEarnPoints: 50,
    itemRedeemPoints: 400,
    itemPrice: 4.00
},
{
    id: 7,
    itemName: 'Broccoli',
    itemEarnPoints: 70,
    itemRedeemPoints: 340,
    itemPrice: 3.40
},
{
    id: 8,
    itemName: 'Pepper',
    itemEarnPoints: 230,
    itemRedeemPoints: 500,
    itemPrice: 5.00
},
{
    id: 9,
    itemName: 'Cake',
    itemEarnPoints: 400,
    itemRedeemPoints: 1000,
    itemPrice: 10.00
},
{
    id: 10,
    itemName: 'Winter Jacket',
    itemEarnPoints: 800,
    itemRedeemPoints: 5000,
    itemPrice: 50.00
},
{
    id: 11,
    itemName: 'Coca Cola 12 Pack',
    itemEarnPoints: 200,
    itemRedeemPoints: 600,
    itemPrice: 6.00
},
{
    id: 12,
    itemName: 'Paper Towels',
    itemEarnPoints: 450,
    itemRedeemPoints: 1200,
    itemPrice: 12.00
}]

