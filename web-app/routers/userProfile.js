'use strict';

//get libraries
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const router = express.Router();
const network = require('/home/priya/Reward_System/reward_system/web-app/network/network.js');


//create express web-app
const app = express();

//bootstrap application settings
app.use(express.static('./public'));
app.use('/scripts', express.static(path.join(__dirname, '/public/scripts')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
//get the libraries to call


router.post('/',function(req,res){
    let accountNumber = req.body.accountnumber;
    let cardId = req.body.cardid;
    console.log('memberData using param - ' + ' accountNumber: ' + accountNumber + ' cardId: ' + cardId);
    let returnData = {};
    var x = new Boolean(false);
    
    network.memberData(cardId, accountNumber)
       .then((member) => {
           //return error if error in response
            if (typeof member === 'object' && 'error' in member && member.error !== null) {
                console.log("User no exist");
                // res.sendFile(path.join(__dirname + '/public/loginSection.html'));
                returnData = null;
                res.redirect('/loginSection')
            } 
            else {
                res.render('example2',{user: "Priya"});
            }
        })
    
})
module.exports  = router;